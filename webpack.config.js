var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel'
      },
      {
        // Transform our own .css files with PostCSS and CSS-modules
        test: /\.css$/,
        loader: 'style-loader!css-loader?localIdentName=[local]__[path][name]__[hash:base64:5]&modules&importLoaders=1',
      },
      {
        test: /\.(jpg|png|gif)$/,
        loader: 'file-loader',
      },
    ]
  },
  devServer: {
    historyApiFallback: true,
  },
};

module.exports = config;
