import React from 'react';
import Logo from './logo.png';
import styles from './styles.css';
import { Link } from 'react-router';

export default class Sidebar extends React.Component {
  render () {
    return (
    	<div className="small-3 columns">
	    	<div className={styles.navbar}>
	    		<img src={Logo} alt="logo" className={styles.logo} />
	    		<Link to='/' className={styles.navLink}>Home</Link>
	    		<Link to='/about' className={styles.navLink}>About</Link>
	    	</div>
    	</div>
    );
  }
}
