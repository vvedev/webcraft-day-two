import React from 'react';
import { render } from 'react-dom';
import Logo from './logo.png';
import styles from './styles.css';
import Sidebar from './sidebar.jsx';
import About from './about.jsx';
import Home from './home.jsx';
import Post from './post.jsx';
import { Router, Route, Link, IndexRoute, browserHistory } from 'react-router';

class App extends React.Component {
  render () {
    return (
    	<div className="row">
	    	<Sidebar />
	    	{ this.props.children }
    	</div>
    );
  }
}

app.propTypes = {
	children: React.PropTypes.node,
};

render((
	<Router history={browserHistory}>
		<Route path='/' component={App}>
			<IndexRoute component={Home} />
			<Route path='about' component={About} />
			<Route path='post' component={Post} >
				<Route path=':postID' component={Post} />
			</Route>
		</Route>
	</Router>
), document.getElementById('app'));
