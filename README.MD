# WEBCRAFT Basic React Template

This is a basic react template featuring webpack minimal configuration.

How to use:

1. Clone this repository using git-bash/terminal/equivalent tool.
2. run this line on your git-bash/terminal/equivalent tool.

	```bash
	npm install
	```
3. After that, run this line.

	```bash
	npm run dev
	```
4. Happy coding:)